﻿using HomeWork41.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HomeWork41.Models
{
    public class Organization : IEntity<int>
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Address { get; set; }
        public string Manager { get; set; }
        public virtual ICollection<Patient> Patients { get; set; }
    }
}
