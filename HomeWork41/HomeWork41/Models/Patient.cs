﻿using HomeWork41.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HomeWork41.Models
{
    public class Patient : IEntity<int>
    {
        public int Id { get; set; }
        [ForeignKey("Organization")]
        public int OrganizationId { get; set; }

        [Required]
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public DateTime DateOfBirth { get; set; }
        public char Sex { get; set; }
        public DateTime AccountDateOfCreation { get; set; }
        
        public virtual Organization Organization { get; set; } 
    }
}
