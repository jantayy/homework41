﻿using HomeWork41.Enums;
using HomeWork41.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HomeWork41.Models
{
    public class Visit : IEntity<int>
    {
        public int Id { get; set; }
        [ForeignKey("Patient")]
        public int PatientId { get; set; }
        public VisitType VisitType { get; set; }
        public DateTime DateOfVisit { get; set; }
        public VisitStatus VisitStatus { get; set; }
        public int Total { get; set; }
        public Patient Patient { get; set; }

    }
}
