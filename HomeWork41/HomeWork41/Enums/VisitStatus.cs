﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork41.Enums
{
    public enum VisitStatus
    {
        NotStarted = 1,
        InProgress = 2,
        Finished = 3
    }
}
