﻿using HomeWork41.Enums;
using HomeWork41.Models;
using System;
using System.Linq;

namespace HomeWork41
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                if (!db.Organizations.Any())
                {
                    var organisation1 = new Organization
                    {
                        Name = "Dolphi",
                        Address = "Bishkek, bokonbaeva 17",
                        Manager = "Alim"
                    };
                    var patient1 = new Patient
                    {
                        Name = "Jantay",
                        Surname = "Kachkynaliev",
                        Patronymic = "Samatovich",
                        Sex = 'M',
                        AccountDateOfCreation = new DateTime(2020, 07, 14),
                        DateOfBirth = new DateTime(2000, 02, 03),
                    };
                    var visit1 = new Visit
                    {
                        DateOfVisit = new DateTime(2021, 03, 05),
                        VisitStatus = VisitStatus.Finished,
                        VisitType = VisitType.Reception,
                        Total = 2000
                    };
                }
            }
        }
    }
}
