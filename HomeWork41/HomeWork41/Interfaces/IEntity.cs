﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork41.Interfaces
{
    public interface IEntity<T>
    {
        public T Id { get; set; }
    }
}
